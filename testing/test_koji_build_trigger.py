import koji
import pytest
from dingus import Dingus
from bunch import Bunch

from jobtriggers import koji_build_msg


@pytest.mark.usefixtures('prepare')
class TestKojiBuildCompletedJobTrigger():
    @pytest.fixture
    def prepare(self, monkeypatch):
        self.ref_instance = 'primary'
        self.ref_new = koji.BUILD_STATES['COMPLETE']
        self.ref_name = 'stage'
        self.ref_version = '4.1.1'
        self.ref_release = '3.fc18'
        self.ref_nvr = '%s-%s-%s' % (self.ref_name, self.ref_version, self.ref_release)

        self._create_msg(self.ref_instance, self.ref_new, self.ref_name, self.ref_release, self.ref_version)

        self.ref_tasks = ['koji_build_task1', 'koji_build_task2']
        self.ref_validarches = ['i386', 'x86_64']
        self.ref_blacklist_releases= ['el5', 'el6']

        stub_hub = Bunch(config=Bunch(get=0))

        self.helper = koji_build_msg.KojiBuildCompletedJobTrigger(stub_hub)
        self.helper.trigger = Dingus()

        koji_build_msg.config.koji_build_completed_tasks = self.ref_tasks
        koji_build_msg.config.valid_arches = self.ref_validarches
        koji_build_msg.config.blacklist_releases = self.ref_blacklist_releases
        koji_build_msg.config.job_logging = False

    def _create_msg(self, ref_instance, ref_new, ref_name, ref_release, ref_version):
        self.ref_message = Bunch(body='{"i": 1,\
                                        "msg": {"build_id": 221146,\
                                                "instance": "%s",\
                                                "new": %d,\
                                                "name": "%s",\
                                                "release": "%s",\
                                                "version": "%s"},\
                                        "timestamp": 1359603469.21164,\
                                        "topic": "org.fedoraproject.prod.buildsys.build.state.change",\
                                        "username": "apache"}' %
                                        (ref_instance, ref_new, ref_name, ref_release, ref_version))

    def test_consume(self):
        self.helper.consume(self.ref_message)

        trigger_calls = self.helper.trigger.trigger_job.calls()

        assert len(trigger_calls) == 4

        assert trigger_calls[0][1] == (self.ref_nvr, koji_build_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[0])
        assert trigger_calls[1][1] == (self.ref_nvr, koji_build_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[1])
        assert trigger_calls[2][1] == (self.ref_nvr, koji_build_msg.ITEM_TYPE, self.ref_tasks[1], self.ref_validarches[0])
        assert trigger_calls[3][1] == (self.ref_nvr, koji_build_msg.ITEM_TYPE, self.ref_tasks[1], self.ref_validarches[1])

    def test_consume_not_primary_instance(self):
        ref_instance = 'ppc'
        self._create_msg(ref_instance, self.ref_new, self.ref_name, self.ref_release, self.ref_version)

        self.helper.consume(self.ref_message)

        trigger_calls = self.helper.trigger.trigger_job.calls()

        assert trigger_calls == []

    def test_consume_notcomplete_state(self):
        ref_new = koji.BUILD_STATES['BUILDING']
        self._create_msg(self.ref_instance, ref_new, self.ref_name, self.ref_release, self.ref_version)

        self.helper.consume(self.ref_message)

        trigger_calls = self.helper.trigger.trigger_job.calls()

        assert trigger_calls == []

    def test_consume_blacklisted_release(self):
        ref_release = '3.el5.1'
        self._create_msg(self.ref_instance, self.ref_new, self.ref_name, ref_release, self.ref_version)

        self.helper.consume(self.ref_message)

        trigger_calls = self.helper.trigger.trigger_job.calls()

        assert trigger_calls == []
