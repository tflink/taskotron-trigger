from dingus import Dingus

from jobtriggers import utils, config


class TestBodhiJobTrigger():
    def setup_method(self, method):
        self.ref_release = '3.el6'

    def test_blacklist_releases_true(self):
        config.blacklist_releases = ['el6', 'el7']
        res = utils.blacklist_releases(self.ref_release)

        assert res

    def test_blacklist_releases_false(self):
        config.blacklist_releases = ['fc20']
        res = utils.blacklist_releases(self.ref_release)

        assert not res
