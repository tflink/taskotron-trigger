import pytest
from dingus import Dingus
from bunch import Bunch

from jobtriggers import koji_tag_msg


@pytest.mark.usefixtures('prepare')
class TestKojiTagChangedJobTrigger():
    @pytest.fixture
    def prepare(self, monkeypatch):
        self.ref_instance = 'primary'
        self.ref_name = 'stage'
        self.ref_version = '4.1.1'
        self.ref_release = '3.fc18'
        self.ref_tag = 'f18-updates-pending'
        self.ref_nvr = '%s-%s-%s' % (self.ref_name, self.ref_version, self.ref_release)

        self.ref_tasks = ['depcheck', 'upgradepath']
        self.ref_validarches = ['i386', 'x86_64']
        self.ref_blacklist_releases= ['el5', 'el6']

        self._create_msg(self.ref_instance, self.ref_name, self.ref_release, self.ref_tag, self.ref_version)

        stub_hub = Bunch(config=Bunch(get=0))
        self.helper = koji_tag_msg.KojiTagChangedJobTrigger(stub_hub)

        self.helper.trigger = Dingus()

        koji_tag_msg.config.koji_tag_changed_tasks = self.ref_tasks
        koji_tag_msg.config.valid_arches = self.ref_validarches
        koji_tag_msg.config.job_logging = False

    def _create_msg(self, ref_instance, ref_name, ref_release, ref_tag, ref_version):
        self.ref_message = Bunch(body='{"i": 1,\
                                        "msg": {"build_id": 221146,\
                                                "instance": "%s",\
                                                "name": "%s",\
                                                "owner": "ralph",\
                                                "release": "%s",\
                                                "tag": "%s",\
                                                "tag_id": 216,\
                                                "user": "bodhi",\
                                                "version": "%s"},\
                                        "timestamp": 1359603469.21164,\
                                        "topic": "org.fedoraproject.prod.buildsys.tag",\
                                        "username": "apache"}' %
                                        (ref_instance, ref_name, ref_release, ref_tag, ref_version))

    def test_delayed_queue(self):
        self.helper.consume(self.ref_message)

        assert self.helper.queued_tags == set([self.ref_tag])

    def test_delayed_consume(self):
        self.helper.queued_tags = [self.ref_tag]

        self.helper.delayed_consume()

        trigger_calls = self.helper.trigger.trigger_job.calls()

        assert len(trigger_calls) == 4

        assert trigger_calls[0][1] == (self.ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[0])
        assert trigger_calls[1][1] == (self.ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[1])
        assert trigger_calls[2][1] == (self.ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[1], self.ref_validarches[0])
        assert trigger_calls[3][1] == (self.ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[1], self.ref_validarches[1])

        assert self.helper.queued_tags == set()

    def test_consume_not_primary_instance(self):
        ref_instance = 'ppc'
        self._create_msg(ref_instance, self.ref_name, self.ref_release, self.ref_tag, self.ref_version)

        self.helper.consume(self.ref_message)

        trigger_calls = self.helper.trigger.trigger_job.calls()

        assert trigger_calls == []

    def test_consume_kojitag_not_pending(self):
        ref_tag = 'f18-updates-testing'
        self._create_msg(self.ref_instance, self.ref_name, self.ref_release, ref_tag, self.ref_version)
        self.helper.consume(self.ref_message)

        assert self.helper.queued_tags == set()

    def test_delayed_consume_upgradepath(self):
        ref_tag = 'f18-updates-testing-pending'
        self.helper.queued_tags = [ref_tag]

        self.helper.delayed_consume()

        trigger_calls = self.helper.trigger.trigger_job.calls()

        assert len(trigger_calls) == 2

        assert trigger_calls[0][1] == (ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[0])
        assert trigger_calls[1][1] == (ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[1])

        assert self.helper.queued_tags == set()

    def test_consume_blacklisted_release(self):
        ref_release = '3.el5.1'
        self._create_msg(self.ref_instance, self.ref_name, ref_release, self.ref_tag, self.ref_version)

        self.helper.consume(self.ref_message)

        assert self.helper.queued_tags == set([])
