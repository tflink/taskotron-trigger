import pytest

from jobtriggers import triggers


class TestTriggers():
    def setup_method(self, method):
        self.ref_triggername = 'StreamTrigger'

    def test_get_valid_trigger(self):
        res = triggers.get_trigger(self.ref_triggername)

        assert isinstance(res, triggers.StreamTrigger)

    def test_get_invalid_trigger(self):
        with pytest.raises(KeyError):
            triggers.get_trigger('NotExistingRandomTrigger')
