Taskotron Job Trigger
=====================

This is very simple code that listens for fedmsg messages of specific types and
triggers jobs in taskotron in buildbot as needed.

Installation
------------

Install the rpm dependencies on the system (only works on Fedora for now)::

  yum install fedmsg fedmsg-hub

Parts of the trigger do need to be installed on the local system::

  python setup.py install

Updating Production Logging Configuration
-----------------------------------------

In /etc/fedmsg.d/logging, add the following handler::

  filelog={
    "class": "logging.handlers.RotatingFileHandler",
    "formatter": "bare",
    "level": "INFO",
    "filename": "/var/log/fedmsg/taskotron-trigger.log",
    "mode": "a",
  }

Then replace the console handler with the filelog handler for the fedmsg
logger so that log messages are sent to a separate file instead of to the
console::

  fedmsg={
    "level": "INFO",
    "propagate": False,
  - "handlers": ["console"],
  + "handlers": ["filelog"],
  },

Running the Trigger
-------------------

Once the fedmsg component has been installed, run the following command in the
working directory with triggerjob.py::

  fedmsg-hub

Setting Up a Dev Environment
----------------------------

Install the following packages if they aren't already installed:

  * koji

  * fedmsg-hub

Create a virtualenv for the trigger, including system site-packages to make sure
that we get koji::

  virtualenv --distribute --system-site-packages env_trigger/

Install requirements::
  source env_trigger/bin/activate
  pip install -r requirements.txt

Install fedmsg and taskotron-trigger into the virtualenv after activating (so
that it picks up the trigger in the virtualenv's site-packages)::

  pip install -U --force-reinstall fedmsg
  pip install -e .

Once this is all done, you can run fedmsg-hub and it will pick up the local
configuration without modifying the local system.

