Name:           taskotron-trigger
Version:        0.3.8
Release:        1%{?dist}
Summary:        Triggering Taskotron jobs via fedmsg.

License:        GPLv2+
URL:            https://fedorahosted.org/fedora-qa
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python-setuptools
Requires:       koji
Requires:       python-bunch
Requires:       python-requests
Requires:       python-twisted
Requires:       fedmsg
Requires:       fedmsg-hub

%description
Triggering Taskotron jobs via fedmsg.


%prep
%setup -q


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# configuration files
mkdir -p %{buildroot}%{_sysconfdir}/taskotron/
install conf/trigger.cfg.example %{buildroot}%{_sysconfdir}/taskotron/trigger.cfg

mkdir -p %{buildroot}%{_sysconfdir}/fedmsg.d/
install fedmsg.d/taskotron-trigger.py %{buildroot}%{_sysconfdir}/fedmsg.d/taskotron-trigger.py

mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d/
install conf/logrotate.d/taskotron-trigger %{buildroot}%{_sysconfdir}/logrotate.d/taskotron-trigger

mkdir -p %{buildroot}%{_localstatedir}/log/taskotron-trigger/

%files
%doc readme.rst
%{python_sitelib}/*

%attr(755,root,root) %{_bindir}/jobrunner

%config(noreplace) %{_sysconfdir}/logrotate.d/taskotron-trigger

%dir %attr(755,fedmsg,fedmsg) %{_localstatedir}/log/taskotron-trigger

%dir %{_sysconfdir}/taskotron
%{_sysconfdir}/fedmsg.d/taskotron-trigger.py*

%config(noreplace) %{_sysconfdir}/taskotron/trigger.cfg

%changelog
* Tue Sep 23 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.8-1
- listen on primary koji instance only
- fix blacklisting releases of the form: '1.el7.1'

* Tue Sep 2 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.7-1
- fix blacklisting releases in koji_tag consumer

* Thu Aug 21 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.6-1
- list trigger.cfg as config file
- fix upgradepath scheduling

* Thu Aug 14 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.5-1
- change to not schedule upgradepath on *-testing-pending

* Mon Jun 30 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.3-1
- switching trigger over to use change_source instead of hacking at force build
- supporting x86_64-only checks

* Tue Jun 24 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.2-1
- change koji tag triggering to use *-pending tags

* Mon Jun 23 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.1-1
- add python-twisted as dep

* Mon Jun 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.0-1
- releasing 0.3.0

* Fri Jun 13 2014 Tim Flink <tflink@fedoraproject.org> - 0.2.1-1
- support triggering with koji_tag on bodhi update change

* Thu May 29 2014 Tim Flink <tflink@fedoraproject.org> - 0.1.2-1
- Fixing typo that broke the jobrunner executable

* Fri May 23 2014 Martin Krizek <mkrizek@redhat.com> - 0.1.1-1
- Add jobrunner script
- Add logrotate conf file

* Fri May 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.1.0-1
- Adding missing deps
- Releasing taskotron-trigger 0.1.0

* Tue Apr 15 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.10-1
- Updating to latest upstream. Triggers buildbot builds based on item arch
- adding koji to requires

* Mon Mar 10 2014 Martin Krizek <mkrizek@fedoraproject.org> - 0.0.9-1
- Initial packaging
