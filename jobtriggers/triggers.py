import requests
import json

from . import config

class BaseTrigger(object):
    """
    Format:
    {
        'item': <name of item - envr, update id, koji tag, etc.>,
        'item_type': <koji_build, koji_tag, bodhi_id>,
        'taskname': <task to be scheduled>,
        'arch': <arch needed for execution, x86_64, i386 etc.>
    }
    """
    def trigger_job(self, item, item_type, taskname, arch):
        raise NotImplementedError


class StreamTrigger(BaseTrigger):
    def trigger_job(self, item, item_type, taskname, arch):
        output = ["StreamTrigger received:",
                    "item: %s" % item,
                    "item_type: %s" % item_type,
                    "taskname: %s" % taskname,
                    "arch: %s" % arch]

        yield '\n'.join(output)


class BuildbotTrigger(BaseTrigger):
    def trigger_job(self, item, item_type, taskname, arch):
        output = ["Buildbot received:",
                    "item: %s" % item,
                    "item_type: %s" % item_type,
                    "taskname: %s" % taskname,
                    "arch: %s" % arch]

        yield '\n'.join(output)

        properties = {
                    'item': item,
                    'item_type': item_type,
                    'taskname': taskname,
                    'arch': arch
                    }


        r = requests.post(config.buildbot_url,
                    data={'author': 'taskotron',
                            'project': 'rpmcheck',
                            'category': arch,
                            'repository': '',
                            'comments': 'build request from taskotron-trigger',
                            'properties': json.dumps(properties)})

        yield "post-hook build request status: %s" % str(r.status_code)


_triggers = {'StreamTrigger': StreamTrigger, 'BuildbotTrigger': BuildbotTrigger}

def get_trigger(trigger_type):
    return _triggers[trigger_type]()
