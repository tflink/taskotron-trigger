import koji
import fedmsg
import fedmsg.encoding

from . import utils, config
from . import JobTrigger

ITEM_TYPE = 'koji_build'

class KojiBuildCompletedJobTrigger(JobTrigger):
    topic = "org.fedoraproject.prod.buildsys.build.state.change"
    jsonify = False
    config_key = 'taskotron.kojibuildcompletedjobtrigger.enabled'

    def __init__(self, *args, **kw):
        super(KojiBuildCompletedJobTrigger, self).__init__(*args, **kw)

    def consume(self, message):
        usable_message = fedmsg.encoding.loads(message.body)

        release = usable_message['msg']['release']
        state = usable_message['msg']['new']
        instance = usable_message['msg']['instance']

        name = usable_message['msg']['name']
        version = usable_message['msg']['version']
        buildid = usable_message['msg']['build_id']

        envr = "%s-%s-%s" % (name, version, release)

        if not instance == 'primary':
            self.log.debug('rejecting message not from primary koji instance (%s from %s)' % (envr, instance))
            return

        if state != koji.BUILD_STATES['COMPLETE']:
            self.log.debug('rejecting message for not being a build complete message (%s, status %s)' % (envr, state))
            return

        if utils.blacklist_releases(release):
            self.log.debug('rejecting message from blacklisted release (%s)' % envr)
            return

        self.log.debug('should be scheduling job for %s' % envr)

        self.trigger_tasks(envr, ITEM_TYPE, config.koji_build_completed_tasks, config.valid_arches)
