import csv
import argparse

from . import triggers, config


TRIGGER = triggers.get_trigger(config.trigger_type)


def run_jobs(jobsfile):
    with open(jobsfile, 'r') as csvfile:
        jobs = csv.reader(csvfile, delimiter=';')
        for job in jobs:
            item, item_type, task, arch = job[1], job[2], job[3], job[4]
            print 'triggering %s for %s on %s' % (task, item, arch)
            output = TRIGGER.trigger_job(item, item_type, task, arch)
            for line in output:
                print line


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('jobsfile')

    args = parser.parse_args()

    return args


def main():
    args = parse_args()
    run_jobs(args.jobsfile)
