import csv
import datetime

from . import config

def log_job(logname, item, item_type, task, arch):
    now = datetime.datetime.now()

    with open(logname, 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow([now, item, item_type, task, arch])

def blacklist_releases(release):
    return any(b in release for b in config.blacklist_releases)
