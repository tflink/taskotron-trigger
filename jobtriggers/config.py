import ConfigParser
import io

defaults = """
[buildbot]
url = http://localhost:8080/buildbot/change_hook

[trigger]
koji_build_completed_tasks = rpmlint
koji_tag_changed_tasks = upgradepath,depcheck
valid_arches = x86_64
koji_url = http://koji.fedoraproject.org/kojihub
type = StreamTrigger
blacklist_releases = el5,el6,el7
job_logging = False
joblog_file = /var/log/taskotron-trigger/jobs.cvs
fuse_delay = 1000 ; seconds
"""

config = ConfigParser.ConfigParser()
config.readfp(io.BytesIO(defaults))
config.read(['/etc/taskotron/trigger.cfg', './conf/trigger.cfg'])


buildbot_url = config.get('buildbot', 'url')

koji_build_completed_tasks = [e.strip() for e in config.get('trigger', 'koji_build_completed_tasks').split(',')]
koji_tag_changed_tasks = [e.strip() for e in config.get('trigger', 'koji_tag_changed_tasks').split(',')]
valid_arches = [e.strip() for e in config.get('trigger', 'valid_arches').split(',')]
koji_url = config.get('trigger', 'koji_url')
trigger_type = config.get('trigger', 'type')
blacklist_releases = [e.strip() for e in config.get('trigger', 'blacklist_releases').split(',')]
job_logging = True if config.get('trigger', 'job_logging') == 'True' else False
joblog_file = config.get('trigger', 'joblog_file')
fuse_delay = int(config.get('trigger', 'fuse_delay')) # seconds
