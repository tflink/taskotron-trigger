import fedmsg.consumers

from . import config, triggers, utils

class JobTrigger(fedmsg.consumers.FedmsgConsumer):
    def __init__(self, *args, **kw):
        super(JobTrigger, self).__init__(*args, **kw)
        self.trigger = triggers.get_trigger(config.trigger_type)

    def trigger_tasks(self, item, item_type, tasks, arches):
        for task in tasks:
            for arch in arches:
                self.log.info('triggering %s for %s on %s', task, item, arch)

                if config.job_logging:
                    try:
                        utils.log_job(config.joblog_file, item, item_type, task, arch)
                    except IOError, e:
                        self.log.warning('Cannot log a job into %s', config.joblog_file)

                output = self.trigger.trigger_job(item, item_type, task, arch)
                for line in output:
                    self.log.info(line)
