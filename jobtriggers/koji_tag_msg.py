from copy import deepcopy

import fedmsg
import fedmsg.encoding
import moksha.hub.reactor

from twisted.internet.task import LoopingCall

from . import config, utils
from . import JobTrigger

ITEM_TYPE = 'koji_tag'

class KojiTagChangedJobTrigger(JobTrigger):
    topic = "org.fedoraproject.prod.buildsys.tag"
    jsonify = False
    config_key = 'taskotron.kojitagchangedjobtrigger.enabled'

    def __init__(self, *args, **kw):
        super(KojiTagChangedJobTrigger, self).__init__(*args, **kw)

        self.queued_tags = set()

        # check for bodhi_to_koji_tags tasks queue every config.fuse_delay seconds
        lc = LoopingCall(self.delayed_consume)
        lc.start(config.fuse_delay)

    def delayed_consume(self):
        if self.queued_tags:
            try:
                for tag in self.queued_tags:
                    # XXX ugly: do not schedule upgradepath for updates-testing-pending
                    tasks = deepcopy(config.koji_tag_changed_tasks)
                    if tag.endswith('testing-pending'):
                        tasks.remove('upgradepath')
                    self.trigger_tasks(tag, ITEM_TYPE, tasks, config.valid_arches)
            finally:
                self.queued_tags = set()
        else:
            self.log.debug("Woke up, but there were no messages.")

    def consume(self, message):
        usable_message = fedmsg.encoding.loads(message.body)

        koji_tag = usable_message['msg']['tag']
        release = usable_message['msg']['release']
        instance = usable_message['msg']['instance']

        if not instance == 'primary':
            self.log.debug('rejecting message not from primary koji instance (%s from %s)' % (koji_tag, instance))
            return

        if not koji_tag.endswith('pending'):
            self.log.debug('rejecting message for not being a pending tag (%s)' % koji_tag)
            return

        if utils.blacklist_releases(release):
            self.log.debug('rejecting message from blacklisted release (%s)' % koji_tag)
            return

        if not len(self.queued_tags):
            self.log.info('setting fuse (%d seconds) for koji_tag %s', config.fuse_delay, koji_tag)
        elif koji_tag not in self.queued_tags:
            self.log.info('adding koji_tag %s to the fuse queue', koji_tag)

        self.queued_tags.add(koji_tag)
